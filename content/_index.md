---
title: Inicio
description: SMM Ilustración y Diseño
images: ["/img/og-image-mujer_1200x1200.jpg"]
---

Soy **Silvia Marecos Medina** y en esta web te voy a explicar quién soy y qué
hago, y te presentaré algunos de mis trabajos.

Si te gustan mis obras y quieres proponerme algún tipo de colaboración o
trabajo, no dudes en ponerte en contacto conmigo. ¡Disfruta de la visita!

#### [¿Quieres saber más sobre mí?](/about/ "Sobre mi")

#### [¿Prefieres ver mi portafolio?](/portfolio/ "Portafolio")

#### [¿Te gustaría leer mi blog?](/post/ "Blog")

&nbsp;
&nbsp;

![Acuarela - Manos de artista](/img/home_banner_separador_800.jpg)

