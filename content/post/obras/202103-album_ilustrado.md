---
title: "Album Ilustrado: La Cabeza del Perro"
description: "Album Ilustrado: La Cabeza del Perro"
date: "2021-03-01T20:00:00+02:00"
publishDate: "2021-03-01T20:00:00+02:00"
images: [ "/post/images/obras/202103-album_ilustrado/libro_album_banner_800x350.jpg" ]
---

![Banner Album Ilustrado "La Cabeza del Perro"](/post/images/obras/202103-album_ilustrado/libro_album_banner_800x350.jpg)

El libro álbum combina la narración visual con la narración verbal a lo largo
de unas paginas, generalmente pocas. El texto y la imagen se unen para crear
un significado y así construir juntos la historia.

<!--more-->

## Las imágenes en un libro álbum

Las imágenes de un libro álbum deben narrar la historia, mostrando una
secuencia de hechos y situaciones relacionados entre sí, donde generalmente
hay un personaje principal (una persona, una animal, un objeto o un ser
imaginario), el protagonista, que realiza acciones o al que le ocurren cosas.


## Proceso de creación a partir de una historia escrita

Para ilustrar un relato escrito en forma de libro álbum, podemos seguir
un proceso de trabajo como este:

- Investigar la historia en sí: quién es su autor y dónde y cuándo se escribió
- Leer varias veces el texto, imaginando todas las posibilidades visuales que
nos ofrece
- Identificar a el o los protagonistas y empezar a dibujarlos dándoles unas
características fácilmente identificables, por ejemplo sus formas o sus
expresiones
- Realizar algunos bocetos rápidos de los personajes y escenarios
- Preparar la narración en imágenes mediante miniaturas o _thumbnails_ para
forzarnos a no entrar en detalles
- Montar un storyboard con la narrativa completa teniendo en cuenta el número
de páginas final
- Refinar y dibujar los personajes, vestimenta, escenarios, etc.
- Escanear los dibujos, procesarlos digitalmente y añadirles los textos
- Imprimir, recortar, montar y unir las páginas (se pueden grapar, pegar o
coser)

¡Ya tenemos la maqueta de nuestro libro álbum ilustrado terminada!


## La Cabeza del Perro

A continuación puedes ver las imágenes y vídeos de la creción de un libro
álbum ilustrado por mí basado en el relato corto "La Cabeza del Perro" de
Arthur Conan Doyle:

&nbsp;
{{% centered %}}
*Estoy en el sillón junto a la chimenea en que crepita el fuego.*  
*Tengo una copa de cognac en la mano derecha.*  
*Con la mano izquierda, caída descuidadamente,*  
*acaricio la cabeza de mi perro...*  
*Hasta que decubro que no tengo perro.*
{{% /centered %}}
&nbsp;

![Album Ilustrado "La Cabeza del Perro" - Bocetos rata](/post/images/obras/202103-album_ilustrado/02_bocetos_raton_800x600.jpg 'Bocetos para la rata')

![Album Ilustrado "La Cabeza del Perro" - Boceto final para la rata](/post/images/obras/202103-album_ilustrado/03_boceto_2_800x600.jpg 'Boceto final de la rata')

![Album Ilustrado "La Cabeza del Perro" - Bocetos](/post/images/obras/202103-album_ilustrado/04_bocetos_libro_album_ilustrado_800x600.jpg 'Más bocetos')

![Album Ilustrado "La Cabeza del Perro" - Storyboard](/post/images/obras/202103-album_ilustrado/01_thumbnails_storyboard_800x600.jpg 'Storyboard con thumbnails o miniaturas')


Vídeo de la encuadernación de la maqueta y el libro álbum terminado:
{{< youtube ISwNanRefTg >}}



