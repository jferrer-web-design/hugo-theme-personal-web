---
title: "Pintar con gouache: el gato Amon-Ra"
description: "Pintar con gouache: el gato Amon-Ra"
date: "2021-09-20T20:00:00+02:00"
publishDate: "2021-09-20T20:00:00+02:00"
images: [ "/post/images/obras/202109-gouache_gato/gouache_banner_800x350.jpg" ]
---

![Banner Pintar con gouache: el gato Amon-Ra](/post/images/obras/202109-gouache_gato/banner-gato-curioso-800x350.jpg)

El gouache o témpera data de la edad Media. Es muy parecida a la acuarela,
aunque más opaca y se diferencia también por su densidad. El gouche puede
utilizarse directamente con pincel o espátula, o incluso con los dedos, sobre
el soporte que utilices, eso ya depende de como  cada uno lo quiera
experimentar.

<!--more-->

## Tipo de papel a utilizar con gouache

Se utiliza el mismo que para acuarela, pero mejor si es de un gramaje alto (de
300g para arriba). También se puede usar gouache sobre cartón, madera, lienzo,
etc. Yo utilizo papel de la marca Arches de grano grueso, 300g y 100% algodón.

Existen diferentes marcas y precios de gouache:

- Winsor & Newton
- Royal Talents
- Royal & Langnickel
- Holbein
- Schmincke
                                      
Lo ideal es comprar gouache para artistas, ya que son de mejor calidad, más
resistentes a la luz y el resultado se ve mejor.

## Qué necesito para pintar

* Lápiz
* Soporte para Gouache, papel gramaje de 300 grs o superior
* Pinceles
* Agua
* Papel absorbente
                     
## Proceso

Primero realizar el boceto con lápiz muy suave. Luego, si se quiere realizar
dibujo con fondo, un elegir un color de gouache y realizar como una imprimación
muy aguada. Vemos los lugares de sombras y luces que tendrá nuestra
ilustración, y así sabremos dónde colocar las luces y las sombras. En
definitiva, iremos trabajando por capas.

![Pintar con gouache - Proceso de trabajo](/post/images/obras/202109-gouache_gato/01_proceso_de_trabajo_600x800.jpg 'Proceso de trabajo')

Lo bueno del gouache es que se puede utilizar un color encima de otro. Una vez
se ha secado la pintura, se tapa fácilmente si lo haces con poca agua, o con la
pintura directamente sobre el dibujo.

![Pintar con gouache - Ilustración terminada](/post/images/obras/202109-gouache_gato/02_amon-ra_600x800.jpg 'Ilustración terminada')

Cuando esté seco, iremos pintando los detalles de nuestra ilustración. Sin
diluir mucho con agua, poco a poco; en este paso necesitamos un pincel más fino
para los detalles y hay que ser muy preciso y delicado.

¡Listo! Ilustración terminada.
                                                               
![Pintar con gouache - Post-procesado con Photoshop](/post/images/obras/202109-gouache_gato/03_amon-ra-photoshop_800x600.jpg 'Post-procesado con Photoshop para ponerle un fondo negro')
