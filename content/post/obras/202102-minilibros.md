---
title: "Libros de Artista"
description: "Libros de Artista"
date: "2021-02-03T20:00:00+02:00"
publishDate: "2021-02-03T20:00:00+02:00"
images: [ "/post/images/obras/202102-minilibros/minilibro_banner_800x350.jpg" ]
---

![Banner Minilibros](/post/images/obras/202102-minilibros/minilibro_banner_800x350.jpg)

Los libros de artista surgen en el siglo XX como una forma de acercar el arte
a todas las clases sociales, cuando artistas de diferentes vanguardias deciden
crear sus obras en formatos pensados para ser tocados y manipulados.

<!--more-->

## El Libro de Artista

Un libro de artista suele ser una pieza única o realizada en series de muy
pocas copias, lo que las hace especiales. El continente de la obra pasa a tener
una gran importancia: su forma tridimensional, su tacto, su olor, su peso... El
artista combina todo ello con el contenido para reforzar el mensaje que desea
transmitir.

Las técnicas y las combinaciones de materiales son casi infinitas, ya que no
existen restricciones: papel, cartón, tela, plástico, collage, origami,
pintura, escultura, fotografía, todo ello acompañado opcionalmente de textos.
El objetivo es que el *observador* de la obra se convierta en *participante* en
la misma, entrando en contacto con la misma con otros sentidos más allá de la
vista.

Para el artista, este nuevo formato ofrece características impensables en los
lienzos clásicos, como la posibilidad de avanzar y retroceder por la obra o las
sensaciones táctiles de sus materiales. La pieza cobra vida en las manos de
quien la contempla y manipula.

El libro de artista es un formato muy versátil en el que tiene cabida cualquier
temática, ya que ofrece una gran libertad creativa.


### Pandemia

En estos tiempos de reclusión, es inevitable pensar en la pandemia...

![Libros de Artista - Pandemia Boceto](/post/images/obras/202102-minilibros/pandemia_0_boceto_800x600.jpg '"Pandemia" - Boceto')
![Libros de Artista - Pandemia Portada](/post/images/obras/202102-minilibros/pandemia_1_800x600.jpg '"Pandemia" - Portada')
![Libros de Artista - Pandemia Delante](/post/images/obras/202102-minilibros/pandemia_2_delante_800x600.jpg '"Pandemia" - Delante')
![Libros de Artista - Pandemia Detras](/post/images/obras/202102-minilibros/pandemia_3_detras_800x600.jpg '"Pandemia" - Detrás')

### Hilo Rojo

El **hilo rojo del destino** es una leyenda muy popular en China y Japón:

{{% centered %}}
*Cuenta la leyenda que un anciano vive en la luna.*  
*Cada noche, sale y busca entre las almas aquellas*  
*que están predestinadas a unirse en la tierra y, cuando las encuentra,*  
*las une con un hilo rojo para que no se pierdan.*  

*Las personas predestinadas a conocerse se encuentran unidas*  
*por un hilo rojo atado al dedo meñique,*  
*que es invisible y permanece atado a estas dos personas,*  
*a pesar de las circunstancias del tiempo y del lugar.*  
*El hilo puede enredarse, tensarse, pero nunca puede romperse.*  

*La vida es un largo camino en el que aprendes una lección cada día.*  
*El destino une a las personas y las convierte en una sola.*  
{{% /centered %}}
&nbsp;

![Libros de Artista - Hilo Rojo Portada](/post/images/obras/202102-minilibros/hilo_rojo_1_portada_800x600.jpg '"Hilo Rojo" - Portada')
![Libros de Artista - Hilo Rojo Delante](/post/images/obras/202102-minilibros/hilo_rojo_2_800x600.jpg '"Hilo Rojo" - Delante')
![Libros de Artista - Hilo Rojo Detrás](/post/images/obras/202102-minilibros/hilo_rojo_3_800x600.jpg '"Hilo Rojo" - Detrás')
![Libros de Artista - Hilo Rojo Desplegable](/post/images/obras/202102-minilibros/hilo_rojo_4_800x600.jpg '"Hilo Rojo" - Desplegable')


