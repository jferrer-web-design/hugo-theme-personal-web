---
title: "Expo: EROS X 3"
description: EROS X 3
date: "2020-01-24T20:00:00+02:00"
publishDate: "2021-01-20T10:00:00+02:00"
images: [ "/post/images/expos/202001-erosx3/banner_erosx3_800x350.jpg" ]
---

![Banner EROS X 3](/post/images/expos/202001-erosx3/banner_erosx3_800x350.jpg)

Exposición de obras eróticas junto a Coco Du Rong y Griselda Roces, organizada
por nosotras mismas y nuestro querido amigo Pep de Espai Sankofa.

<!--more-->

## EROS X 3 - Coco, Griselda, Silvia

Cuando coincidimos  en el [Festival de Artes Eróticas AEFEST](/post/expos/201904-aefest/)
de 2019, [Coco](https://www.instagram.com/mutumurong2.0/),
[Griselda](http://griseldarocesdibujos.blogspot.com)
y yo hablamos de la posibilidad de hacer algún proyecto juntas en el futuro,
también relacionado con el erotismo, que tan presente está en nuestras obras.

{{< figure-width image="/post/images/expos/202001-erosx3/coco_griselda_silvia.jpg" width=60 title="Coco Du Rong, Griselda Roces y yo" text="Coco Griselda Silvia" >}}

Un día, hablando con mi buen amigo Pep de la ONGD Sovint en su local, el
[Espai Sankofa](https://sovint.org/el-espacio), surgió la idea de montar allí
una exposición. Tras comentarlo con Coco y con Griselda, nos pusimos manos a la
obra.


## Organización de la exposición

Cada una de nosotras seleccionó algunas de sus obras recientes y Griselda se
ofreció a hacer también una lectura para presentar su libro de poesía erótica
"Se desanuda la lengua".

Fijamos unas fechas con el Espai Sankofa y Coco se encargó de diseñar un
espectacular cartel para la exposición:

![EROS X 3 - Cartel promocional](/post/images/expos/202001-erosx3/cartel_exposicion_600x800.jpg "Cartel promocional de la exposición EROS X 3 (Coco Du Rong)")

Empezamos a promocionar la exposición con carteles y por las redes sociales, y
llegó el día del montaje:

![EROS X 3 - Montaje](/post/images/expos/202001-erosx3/montaje_erosx3_600x800.jpg "Montando la exposición con Pep (que parece hacer todo el trabajo)")

Mi serie incluyó las siguientes obras:

* **Lectura Erótica** (para cartel promocional de los Tardeos Eróticos)
* **Juliette** (de la portada del libro "Juliette o las Prosperidades del Vicio)
* **Tardeo Erótico** (para cartel promocional de los Tardeos Eróticos)
* **Bijou** (inspirado por lecturas de los Tardeos Eróticos)
* **Tarde de Invierno**
* **Regalo** (inspirado por lecturas de los Tardeos Eróticos)
* **La Dama del Lirio**

Puedes ver algunas de estas obras en más detalle en la ["Serie Erotismo" de mi
portafolio](/portfolio/obras-erotismo/).

![EROS X 3 - Cuadros Silvia 1](/post/images/expos/202001-erosx3/cuadros_1_800.jpg "Algunas de mis obras expuestas")

![EROS X 3 - Cuadros Silvia 2](/post/images/expos/202001-erosx3/cuadros_2_800.jpg "Algunas obras más")

## Inauguración


El viernes 24 de Enero a las 20h hicimos una fiesta de inauguración en la que
reunimos a gran cantidad de amigos. Para la ocasión, nuestra querida
[Julieta Gutiérrez](https://julietagutierrez.com/)
preparó un discurso de apertura en el que resumió el papel del erotismo y la
sexualidad en el arte a lo largo de la historia, para a continuación presentar
a las tres artistas.

![EROS X 3 - Inauguracion Silvia](/post/images/expos/202001-erosx3/inauguracion_1_800x600.jpg "Mis obras y yo el día de la inauguración")

![EROS X 3 - Inauguracion: Silvia, Julieta, Coco, Pep y Griselda](/post/images/expos/202001-erosx3/inauguracion_2_800x600.jpg "Silvia, Julieta, Coco, Pep y Griselda")


**Aprovecho para agradecer desde aquí a toda la gente que nos ayudó a organizar
esta exposición, y muy especialmente a Pep y la gente del Espai Sankofa.**