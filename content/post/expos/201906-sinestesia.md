---
title: "Expo: Pintar la Música"
description: Sinestesia - Pintar la Música (2019)
date: "2019-06-14T18:00:00+02:00"
publishDate: "2021-01-14T18:00:00+02:00"
images: [ "/post/images/expos/201906-sinestesia/cartel_banner_800x350.jpg" ]
---

![Banner Sinestesia](/post/images/expos/201906-sinestesia/cartel_banner_800x350.jpg)

Proyecto colaborativo organizado por la Academia de Arte GAIA y el
Conservatorio de Música José Iturbi que tiene como tema principal la
sinestesia.


<!--more-->

## Sinestesia y la relación entre música y pintura

La sinestesia es la *activación automática e involuntaria de una vía sensorial
adicional en respuesta a estímulos concretos* que experimentan ciertas personas.
Serían ejemplos de sinestesia que una persona fuera capaz de ver un color al
escuchar una nota musical o que sientiera un sabor dulce cuando le tocaran el
pelo.


## Proyecto "Pintar la Música"

De este interesante concepto surge el proyecto **"Pintar la música"**,
organizado conjuntamente por la academia de arte [GAIA](https://www.gaiarestauracion.com/)
y el [Conservatorio Municipal de Música José Iturbi de Valencia](https://www.facebook.com/IturbiVLC/).

![Cartel del Proyecto Pintar la Música](/post/images/expos/201906-sinestesia/cartel_600x.jpg "Cartel del Proyecto Pintar la Música")

El proyecto consistía en asignar a cada artista, todos alumnos de GAIA, una
canción que debía escuchar e interiorizar, y para luego plasmar los
sentimientos que le evocaba en una obra.

Durante cuatro meses, las obras estuvieron expuestas en el Conservatorio, en
una sala donde podían escucharse las diferentes canciones seleccionadas.


## El círculo de la Vida

La canción que me asignaron fue "Circle of Life" interpretada por Elton John
para la banda sonora de la película "El Rey León".

### Ficha musical

**Título**: Circle of Life  
**Autor**: Elton John (música) y Tim Rice (letra)  
**Intérprete**: Elton John  
**Año**: 1994  
**Género**: Pop, Soft rock  

**Instrumentos**: percusión, piano, saxofón  
**Tipo de formación**: solista, coro  
**Timbre del sonido**: suave, envolvente  
**Percepción general del volumen**: suave, moderado, cambios suaves  


### Escucha

En las primeras escuchas, mis primeras impresiones fueron

{{% centered %}}
***tristeza***  
***melancolía***  
***conmovedora***  
***elevación***  
***esperanza***  
***fe***  
***vida***  
***renacimiento***  
***fluir***  
***dar***  
***encontrar nuestro lugar en el mundo***  
{{% /centered %}}

Cerrando los ojos, en mi cabeza imaginaba colores intensos e impactantes, así
como formas redondas.


### Proceso creativo

En lo primeros bocetos, una de las primeras cosas que dibujé fueron olas en el
mar, en concreto una ola que describía un arco hasta volver al punto donde
empezó:

![Circle of Life - Proceso creativo](/post/images/expos/201906-sinestesia/proceso_creativo_1_800x600.jpg "Primeros bocetos - Ola circular")

A partir de ahí, arrancó todo el proceso: elaboración de bocetos, ilustración
de cada una de las partes que compondrían la obra y finalmente la maquetación
digital con Photoshop. 

Todo el proceso quedó plasmado en un vídeo tipo *making-of* que puedes
encontrar [un poco más abajo]({{< relref "#preparación-de-la-exposición" >}}).

![Circle of Life - Proceso creativo](/post/images/expos/201906-sinestesia/proceso_creativo_primer_boceto_800x600.jpg "Primer boceto del motivo principal")

![Circle of Life - Proceso creativo](/post/images/expos/201906-sinestesia/proceso_creativo_3_mujer_600x800.jpg "Mujer: boceto / boceto / procesado digitalmente")

![Circle of Life - Proceso creativo](/post/images/expos/201906-sinestesia/proceso_creativo_2_medusas_800x600.jpg "Medusas: boceto / dibujo / procesado digitalmente")

![Circle of Life - Proceso creativo](/post/images/expos/201906-sinestesia/proceso_creativo_4_diente_de_leon_600x800.jpg "Diente de León - dibujo ya procesado digitalmente")



### La obra: "Circle of Life"

La obra muestra el agua, símbolo de la vida, el océano, animales, naturaleza,
y el motivo central es una figura humana en posición fetal, de cuya columna
vertebral nace un diente de león que desprende sus semillas en el viento. En el
otro lado, unas medusas parecen surgir de las costillas de la mujer, escapar 
del mar y volar hacia el infinito. Todo ello enmarcado en una ola circular.

![Cuadro Circle of Life](/post/images/expos/201906-sinestesia/obra_circle_of_life_2_800x600.jpg "La obra terminada: Circle of Life")


## La exposición

![Circle of Life expuesto](/post/images/expos/201906-sinestesia/foto_exposicion_600x800.jpg "Circle of Life en la exposición")


Como parte del proyecto se nos pidió que realizáramos un **video del proceso de
creación de la obra**. 

Por cuestiones de copyright, en esta página solo podéis ver una versión sin la
música, pero **podéis verlo con música entrando en Youtube**
[**en este enlace**](https://youtu.be/n6Ml_CvFdOI).

***Making-of* "Circle of Life"** - Versión sin música (por limitaciones de copyright):
{{< youtube bq7BEBd_Tfg >}}

Los vídeos de cada obra se proyectaron en los conciertos de inauguración y
clausura de la exposición, donde los alumnos interpretaban en vivo las
canciones seleccionadas.

Interpretación de "Circle of Life (Ciclo sin fín)" por el Coro de los alumnos del
Conservatorio José Iturbi durante el concierto de clausura de la exposición:
{{< youtube vE-Z36ZwlVo >}}

