---
title: "Expo: Flash Figures"
description: Flash Figures - Celebrating the Figure on Paper (2020)
date: "2020-02-20T19:00:00+02:00"
publishDate: "2021-01-22T18:00:00+02:00"
images: [ "/post/images/expos/202002-flash-figures/flash_figures_banner_800x350.jpg" ]
---

![Banner Flash Figures](/post/images/expos/202002-flash-figures/flash_figures_banner_800x350.jpg)

Exposición colectiva en torno a la figura humana en la galería RW Project de Ruzafa.

<!--more-->

## Flash Figures - Celebrating the Figure on Paper

Organizada por Katherine Brown y Emma Shapiro en la galería
[RW Project](https://kristingracie.com/the-rw-project/), la exposición reunió
obras más de veinte artistas internacionales, cuyos temas giraban en torno al
desnudo, al exibicionismo (_flashing_) y al erotismo.

En [este artículo](https://awayfromthegrain.com/2020/02/16/flash-figures-celebrating-the-figure-on-paper/)
(en inglés) podéis leer cómo surge la idea de organizar esta exposición.

## Convocatoria

A principios de Febrero, alguien me hizo llegar una convocatoria que buscaba
reunir obras relacionadas con "plasmar la figura humana en papel":

{{< figure-width image="/post/images/expos/202002-flash-figures/flash_figures_convocatoria_800x600.jpg" width=75 title="Convocatoria para la exposición" text="Convocatoria para la exposición" >}}

Al ser uno de mis temas predilectos, decidí enviarles algunos de mis últimos
trabajos. La exposición se organizó muy rápido, y tan solo unos días más tarde
me confirmaron mi participación.

## La exposición

La inauguración tuvo lugar el jueves 20 de Febrero en RW Project, una pequeña
pero acogedora galería en el barrio de Ruzafa de Valencia, y congregó a un gran
número de personas de múltiples nacionalidades.

{{< figure-width image="/post/images/expos/202002-flash-figures/flash_figures_cartel_800x600.jpg" width=75 title="Cartel promocional de la exposición" text="Cartel promocional de la exposición" >}}

![Flash Figures - Exposición Obras](/post/images/expos/202002-flash-figures/flash_figures_expo_1_800x600.jpg "Flash Figures en RW Project - a la derecha mis dos obras seleccionadas")

Al poco de empezar la exposición, me llevé una grata sorpresa pues alguien
decidió comprar una de mis obras, "La Dama del Lirio".

![Flash Figures - Exposición Obras](/post/images/expos/202002-flash-figures/flash_figures_expo_2_800x600.jpg "Arriba: Noche de Invierno  /  Abajo: La Dama del Lirio")

En [este enlace](https://awayfromthegrain.com/2020/02/29/flash-figures-celebrating-the-figure-on-paper-2/)
podéis encontrar una crónica de la exposición acompañada de fotos del resto de
las obras expuestas.

