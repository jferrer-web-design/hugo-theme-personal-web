---
title: Storyboard "The captured bird"
description: Storyboard "The captured bird"
date: "2018-01-01T19:47:09+02:00"
jobDate: 2018
work: [storyboard, acuarela, tinta]
designs: [acuarela, tinta]
# techs: [photoshop]
category: "libros"
thumbnail: libros-storyboard/storyboard_0_800x400.jpg
---

**Proyecto personal de creación de un Storyboard para el cortometraje "The Captured Bird".**

Guión gráfico para el cortometraje "The Captured Bird" de 2012, dirigido por Jovanka Vuckovic y producido por Guillermo del Toro (entre otros).

#### Sinopsis

Una niña sigue el rastro de un líquido oscuro hasta un edificio y presencia el nacimiento de cinco criaturas que amenazan al mundo.

{{< add-flickity folder="slideshow" title="Storyboard - The Captured Bird" wrapAround=false >}}
