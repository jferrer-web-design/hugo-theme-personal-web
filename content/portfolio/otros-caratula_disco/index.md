---
title: Packaging para CD
description: Proyecto de packaging para CD
date: "2020-07-02T19:47:09+02:00"
jobDate: 2020
work: [packaging, tinta, rotuladores, digital]
designs: [tinta, rotuladores]
techs: [photoshop]
category: "otros"
thumbnail: otros-caratula_disco/caratula_disco_0_800x400.jpg
---

### Proyecto de packaging para CD

![Packaging para vinilo](caratula_disco_1_packaging.jpg "Packaging para edición en vinilo")

![Disco](caratula_disco_2_cd.jpg "Diseño del CD")
