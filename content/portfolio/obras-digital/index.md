---
title: Obra digital
description: Obra digital
date: "2022-01-01T19:47:09+02:00"
jobDate: 2022/2023
work: [acuarela digital, digital]
designs: [acuarela digital, digital]
techs: [photoshop]
category: "obras"
thumbnail: obras-digital/digital_0_800x400.jpg
---


![Obras - Witch and cats (2022)](digital_4_witch_and_cats_2022.jpg "Witch and cats (2022)")

![Obras - Raíces (2022)](digital_3_raices_2022.jpg "Raíces (2022)")

![Obras - Primavera (2022)](digital_2_primavera_2022.jpg "Primavera (2022)")

![Obras - Mujer Tigre (2022)](digital_1_mujer_tigre.jpg "Mujer Tigre (2022)")

