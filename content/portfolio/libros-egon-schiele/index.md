---
title: Biografía ilustrada de Egon Schiele
description: Biografía ilustrada del pintor Egon Schiele
date: "2018-01-02T19:47:09+02:00"
jobDate: 2018
work: [libro ilustrado, acuarela, tinta, grafito, pan de oro, collage, digital]
designs: [acuarela, tinta, grafito, pan de oro, collage]
techs: [photoshop]
category: "libros"
thumbnail: libros-egon-schiele/egon_0_800x400.jpg
---

![Biografía ilustrada de Egon Schiele - Portada del libro](egon_1_portada.jpg)

**Proyecto personal de creación de un libro biográfico ilustrado del pintor austriaco Egon Schiele, con textos y dibujos propios.**

Realicé este trabajo sobre la vida de uno de mis artistas preferidos, por su estilo tan característico: sus dibujos tan lineales, la expresividad de sus retratos y el erotismo de sus desnudos, que reflejan emociones, sentimientos y hábitos sexuales que escandalizaron en su época y siguen escandalizando hoy en día.

***Pendiente de edición.***

{{< add-flickity folder="slideshow" title="Preview del libro" wrapAround=false >}}

#### Detalles de las ilustraciones

![Biografía ilustrada de Egon Schiele - Detalles de las ilustraciones](egon_2_proceso.jpg "Ilustraciones originales antes de la maquetación")
