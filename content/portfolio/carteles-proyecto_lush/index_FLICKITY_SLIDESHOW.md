---
title: Diseño para Cosméticos Lush
description: Proyecto Cosméticos Lush
date: "2020-09-02T19:47:09+02:00"
jobDate: 2020
work: [proyecto personal, packaging, acuarela, tinta, digital]
designs: [acuarela, tinta]
techs: [photoshop]
category: "publicidad"
thumbnail: carteles-proyecto_lush/proyecto_lush_0_800x400.jpg
---

**Proyecto personal de diseño de carteles publicitarios y packaging para la marca de cosméticos Lush "*fresh handmade cosmetics*".**

Las ilustraciones tienen como temas principales la naturaleza, los animales en peligro de extinción y el océano.

{{< add-flickity folder="slideshow" title="Slideshow Lush" >}}

XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

![Campaña Lush 0](/img/portfolio/lush_0.jpg "Póster para campaña publicitaria de Lush")
![Campaña Lush 1](/img/portfolio/lush_1.jpg "Póster para campaña publicitaria de Lush")
![Campaña Lush 2](/img/portfolio/lush_2.jpg "Póster para campaña publicitaria de Lush")




