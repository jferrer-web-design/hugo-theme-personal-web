---
title: Diseño para Cosméticos Lush
description: Proyecto Cosméticos Lush
date: "2020-09-02T19:47:09+02:00"
jobDate: 2020
work: [proyecto personal, packaging, acuarela, tinta, digital]
designs: [acuarela, tinta]
techs: [photoshop]
category: "publicidad"
thumbnail: carteles-proyecto_lush/proyecto_lush_0_800x400.jpg
---

**Proyecto personal de diseño de carteles publicitarios y packaging para la marca de cosméticos Lush "*fresh handmade cosmetics*".**

Las ilustraciones tienen como temas principales la naturaleza, los animales en peligro de extinción y el océano.

{{< add-flickity folder="slideshow" title="Carteles promocionales" >}}

#### Otros trabajos

![Proyecto Lush - Catálogo - portada](lush_1_catalogo_portada.jpg "Catálogo de productos - portada")

![Proyecto Lush - Catálogo - interior](lush_2_catalogo.jpg "Catálogo de productos")

![Proyecto Lush - Packaging - cremas](lush_3_packaging.jpg "Packaging - Botes de crema")

![Proyecto Lush - Merchandising - pañuelos para furoshiki](lush_4_furoshiki.jpg "Merchandising - Pañuelos para furushiki")

![Proyecto Lush - Merchandising - camiseta](lush_5_camiseta.jpg "Merchandising - Camiseta")