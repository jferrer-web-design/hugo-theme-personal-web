---
title: Obras varias
description: Obras varias
date: "2023-01-01T19:47:09+02:00"
jobDate: 2019/2020/2021/2022/2023
work: [ilustración, acuarela, acrílico, rotulador, tinta, digital]
designs: [acuarela, acrílico, rotulador, tinta]
techs: [photoshop]
category: "obras"
thumbnail: obras-varios/obras_0_800x400.jpg
---

![Obras - Mariposa y cráneo (2023)](obras_14_mariposa_y_craneo_2023.jpg "Mariposa y cráneo (2023)")

![Obras - Ave naturaleza (2023)](obras_13_ave_naturaleza_2023.jpg "Ave naturaleza (2023)")

![Obras - Primavera (2023)](obras_12_primavera_2023.jpg "Primavera (2023)")

![Obras - Alas para volar y un equipaje ligero (2023)](obras_11_Alas_para_volar_y_un_equipaje_ligero_2023.jpg "Alas para volar y un equipaje ligero (2023)")

![Obras - Buscando a hipnos (2021)](obras_10_buscando_a_hipnos_2021_2.jpg "Buscando a hipnos (2021)")

![Obras - Nix, la Diosa de la noche (2022)](obras_8_nix_diosa_de_la_noche_2021.jpg "Nix, la Diosa de la noche (2022)")

![Obras - Pies y Naturaleza (2022)](obras_7_pies_y_naturaleza.jpg "Pies y Naturaleza (2021)")

![Obras - La Dama del Lirio](obras_1_mascara_gas.jpg "La Dama del Lirio (2020)")

![Obras - Mujer pulpo](obras_4_mujer_pulpo.jpg "Mujer pulpo (2020)")

![Obras - Náyade 1](obras_5_nayades_1.jpg "Náyade 1 (2020)")

![Obras - Náyade 2](obras_6_nayades_2.jpg "Náyade 2 (2020)")

![Obras - Tardeos eróticos](obras_2_tardeo.jpg "Ilustración para la promoción de los Tardeos Eróticos (2019)")

![Obras - Faro](obras_3_faro.jpg "Faro (2019)")



