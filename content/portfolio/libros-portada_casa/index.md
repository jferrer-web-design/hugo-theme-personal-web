---
title: Portada para "Aquella casa"
description: Portada para "Aquella casa"
date: "2020-08-12T19:47:09+02:00"
jobDate: 2020
work: [portada de libro, lápices polychromos, grafito, tinta, digital]
designs: [lápices polychromos, grafito, tinta]
techs: [photoshop]
category: "libros"
thumbnail: libros-portada_casa/aquella_casa_0_800x400.jpg
---

![Aquella casa - Portada del libro](aquella_casa_1_mockup.jpg)

**Proyecto personal de diseño de portada para el libro "Aquella casa" de Tobías Moudelle.**

La ilustración de portada representa la casa protagonista de este libro de terror.


![Aquella casa - Proceso de elaboración](aquella_casa_2_proceso.jpg "Trabajos del proceso de elaboración")

