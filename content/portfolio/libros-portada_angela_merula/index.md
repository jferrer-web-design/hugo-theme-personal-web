---
title: Portada para "Merula" de Ángela Cantero
description: Portada para "Merula" de Ángela Cantero
date: "2023-06-01T19:47:09+02:00"
jobDate: 2023
work: [portada de libro, tinta, acuarela, digital]
designs: [acuarela, tinta]
techs: [photoshop]
category: "libros"
thumbnail: libros-portada_angela_merula/merula_0_800x400.jpg
---

![Merula - Portada del libro](merula_1.jpg "Portada del libro")

**Ilustración para la portada para el libro "Merula. Diosa, humana y bruja" de Ángela Cantero.**

Ángela Cantero ([@angelaycafe](https://www.instagram.com/angelaycafe/)) volvió a confiar en mí para que le hiciese unas ilustraciones para la portada y contraportada de su nuevo libro "Merula. Diosa, humana y bruja".

![Merula - Contraportada del libro](merula_2.jpg "Contraportada")

Para la presentación del libro, también se diseñaron unos marcapáginas.

![Merula - Marcapáginas](merula_3.jpg "Marcapáginas")
