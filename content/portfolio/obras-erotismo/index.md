---
title: Serie Erotismo
description: Serie Erotismo
date: "2019-11-02T19:47:09+02:00"
jobDate: 2018/2019/2020
work: [ilustraciones, dibujos, pinturas]
designs: [varias técnicas]
techs: [photoshop]
category: "obras"
thumbnail: obras-erotismo/erotismo_0_800x400.jpg
---

### **Exposiciones**

He participado en numerosas exposiciones colectivas relacionadas con el erotismo y la sensualidad, ya que gran parte de mi obra artística gira en torno a ellos. Estas son algunas exposiciones recientes:  
&nbsp;
- **AEFEST - 1er Festival de Artes Eróticas de Valencia**  
  en [Jarit](https://www.jarit.org/) y [Lluerna Espai Sociocultural](https://es-es.facebook.com/lluernaespai/), Valencia (2019)

- **Exposicion "Flash figures: Celebrating the figure on paper"**  
  en [The RW Project](https://kristingracie.com/the-rw-project), Valencia (2020)

- **Exposición EROS X 3** (con Coco Du Rong y Griselda Roces)  
  en [Sankofa Espai Intercultural](https://sovint.org/), Valencia (2020)

&nbsp;

![Serie Erotismo - Noche de Invierno](erotismo_1_noche_de_invierno_600x800.jpg "Noche de Invierno")

![Serie Erotismo - La Dama del Lirio](erotismo_2_dama_del_lirio_600x800.jpg "La Dama del Lirio")

![Serie Erotismo - Mujer y Luna dorada](erotismo_5_mujer_luna_dorada_600x800.jpg "Mujer y Luna dorada")

### **Tardeos Eróticos**

Participo frecuentemente en los **"Tardeos Eróticos"**, actividad cultural donde se realizan lecturas teatralizadas de relatos eróticos, mientras yo **dibujo en vivo** plasmando sobre el papel algunas de las historias. 

![Serie Erotismo - Regalo](erotismo_3_regalo_600x800.jpg "Regalo (inspirada por las lecturas de los Tardeos Eróticos)")

![Serie Erotismo - Bijou](erotismo_4_bijou_600x800.jpg "Bijou (inspirada por las lecturas de los Tardeos Eróticos)")

![Serie Erotismo - Lectura Erótica](erotismo_6_lectura_erotica_800x600.jpg "Lectura erótica (para cartel promocional de los Tardeos Eróticos)")

![Serie Erotismo - Tardeo Erótico](erotismo_7_tardeo_erotico_800x600.jpg "Tardeo Erótico (para cartel promocional de los Tardeos Eróticos)")

