---
title: Portada para "Juliette o las prosperidades del vicio"
description: Portada para "Juliette o las prosperidades del vicio"
date: "2020-03-15T19:47:09+02:00"
jobDate: 2020
work: [proyecto personal, portada de libro, acuarela, tinta, digital]
designs: [acuarela, tinta]
techs: [photoshop]
category: "libros"
thumbnail: libros-portada_juliette/portada_juliette_0_800x400.jpg
---

![Juliette - Portada del libro](juliette_1_mockup.jpg)

**Proyecto personal de diseño de portada para el libro "Juliette o las prosperidades del vicio" del Marqués de Sade.**

La ilustración de la portada representa a Juliette entregándose sin escrúpulos al vicio y al crimen, representados por su desnudez y el fondo rojo, color de la pasión y de la sangre.

