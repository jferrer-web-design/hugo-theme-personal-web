---
title: Festival de Cine Asiático
description: Festival de Cine Asiático
date: "2019-05-02T19:47:09+02:00"
jobDate: 2019
work: [proyecto personal, acuarela, tinta, digital]
designs: [acuarela, tinta]
techs: [photoshop]
category: "publicidad"
thumbnail: carteles-cine_asiatico/cine_asiatico_0_800x400.jpg
---

**Proyecto personal de diseño de cartelería y publicidad para un festival de cine asiático en el Centro de Creación Contemporánea Matadero de Madrid.**

Las ilustraciones tienen como inspiración el cine asiático contemporáneo, en concreto de Japón, China, Hong Kong, Taiwán y Corea del Sur, países donde ha sido uno de los fenómenos de masas mas importantes del siglo. Sus películas nos presentan su cultura, sus costumbres y su historia, con gran destreza para combinar géneros.


{{< add-flickity folder="slideshow" title="Cartel del festival y carteles de películas" >}}

#### Fotomontaje

![Festival de Cine Asiático - Montaje de cartelería](cine_asiatico_1_carteles_matadero.jpg "Montaje de cartelería para el Festival de Cine Asiático")

#### Diseño de entradas y folleto para el festival

![Festival de Cine Asiático - Entradas del festival](cine_asiatico_2_entrada.jpg "Billetería para el Festival de Cine Asiático")

![Festival de Cine Asiático - Folleto informativo](cine_asiatico_3_folleto1.jpg "Folleto informativo del Festival de Cine Asiático - Logotipo")

![Festival de Cine Asiático - Plano del recinto del festival](cine_asiatico_4_folleto2.jpg "Folleto informativo del Festival de Cine Asiático - Plano")
