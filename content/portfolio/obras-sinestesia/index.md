---
title: El círculo de la vida
description: El círculo de la vida
date: "2019-11-02T19:47:09+02:00"
jobDate: 2019
work: [concurso, cuadro, grafito, acuarela, tinta, digital]
designs: [grafito, acuarela, tinta]
techs: [photoshop]
category: "obras"
thumbnail: obras-sinestesia/sinestesia_0_800x400.jpg
---

**Obra inspirada por la canción "Circle of Life" de Elton John, para la exposición
colectiva "Sinestesia - Pintar la música" en el Conservatorio de Municipal de Música
"José Iturbi" de Valencia.**

El tema de la exposición era la sinestesia: escuchar música y representar gráficamente lo que percibimos y sentimos a través de ella.

![El círculo de la vida](sinestesia_1_cuadro.jpg "El Círculo de la Vida")

#### Proceso de creación de la obra

**Puedes ver una versión del vídeo con la música de Elton John accediendo directamente a youtube desde [este enlace](https://youtu.be/n6Ml_CvFdOI).**

Versión sin música (por limitaciones de copyright):
{{< youtube bq7BEBd_Tfg >}}
