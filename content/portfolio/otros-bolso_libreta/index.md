---
title: Merchandising "Sueño"
description: Merchandising basado en la obra "Sueño"
date: "2018-09-02T19:47:09+02:00"
jobDate: 2018
work: [merchandising, grafito, tinta]
designs: [grafito, tinta]
techs: [photoshop]
category: "otros"
thumbnail: otros-bolso_libreta/bolso_libreta_0_800x400.jpg
---

**Merchandising basado en la obra "Sueño"**

![Merchandising](bolso_libreta_1_foto.jpg "Bolso y libreta inspirados en la obra Sueño")
