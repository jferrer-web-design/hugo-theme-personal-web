---
title: Libro ilustrado infantil
description: Ilustraciones para un libro infantil
date: "2019-04-12T19:47:09+02:00"
jobDate: 2019
work: [libro ilustrado, acuarela, tinta, grafito, digital]
designs: [acuarela, tinta, grafito]
techs: [photoshop]
category: "libros"
thumbnail: libros-infantil_ilustrado/infantil_ilustrado_0_800x400.jpg
---

**Ilustración de las historias "Búho" y "Beluga" para un libro infantil creado por una ONG.** 

![Libro ilustrado infantil - Búho](libro_infantil_1_buho.jpg "Ilustración para la historia Búho")

![Libro ilustrado infantil - Belugo](libro_infantil_2_beluga.jpg "Ilustración para la historia Beluga")

