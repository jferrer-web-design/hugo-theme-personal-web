---
title: Merchandising "Janis Joplin"
description: Merchandising basado en la obra "Janis Joplin"
date: "2020-09-22T19:47:09+02:00"
jobDate: 2020
work: [merchandising, rotuladores, bolígrafo, digital]
designs: [rotuladores, bolígrafo]
techs: [photoshop]
category: "otros"
thumbnail: otros-camiseta_janis/camiseta_janis_0_800x400.jpg
---

### Merchandising basado en la obra "Janis Joplin"

![Camiseta Janis Joplin](camiseta_janis_1.jpg "Camiseta Janis Joplin")




