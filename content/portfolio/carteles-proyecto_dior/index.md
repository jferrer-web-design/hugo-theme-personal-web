---
title: "Dior: ilustraciones para revista"
description: "Dior: ilustraciones para revista"
date: "2018-08-02T19:47:09+02:00"
jobDate: 2018
work: [proyecto personal, prensa, acuarela, tinta, digital]
designs: [acuarela, tinta]
techs: [photoshop]
category: "publicidad"
thumbnail: carteles-proyecto_dior/proyecto_dior_0_800x400.jpg
---

**Proyecto personal de ilustraciones para portada y artículo sobre Christian Dior en la revista Vogue.**

Ilustraciones de moda inspiradas por la colección primavera/verano de alta costura de Cristhian Dior en París, maquetadas como portada y artículo de la revista Vogue.


![Proyecto Dior - Portada Vogue](revista_dior_1_portada.jpg "Montaje portada revista Vogue")

![Proyecto Dior - Artículo](revista_dior_2_articulo.jpg "Montaje artículo revista Vogue")

![Proyecto Dior - Ilustración para artículo](revista_dior_3_ilustracion_articulo.jpg "Ilustración para el artículo")
