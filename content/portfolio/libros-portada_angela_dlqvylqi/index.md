---
title: Portada para "De lo que vivo y lo que invento" de Ángela Cantero
description: Portada para "De lo que vivo y lo que invento" de Ángela Cantero
date: "2022-02-01T19:47:09+02:00"
jobDate: 2022
work: [portada de libro, tinta, acuarela, digital]
designs: [acuarela, tinta]
techs: [photoshop]
category: "libros"
thumbnail: libros-portada_angela_dlqvylqi/dlqvylqi_0_800x400.jpg
---

![De lo que vivo y lo que invento - Portada del libro](dlqvylqi_1.jpg "Portada del libro")

**Diseño de portada para el libro "De lo que vivo y lo que invento" de Ángela Cantero.**

La escritora Ángela Cantero ([@angelaycafe](https://www.instagram.com/angelaycafe/)) me encargó el diseño de la portada de su libro "De lo que vivo y lo que invento". La ilustración que eligió para la portada forma parte de la colección "Escenas BDSM".

![De lo que vivo y lo que invento - Contraportada del libro](dlqvylqi_2.jpg "Contraportada")

Para la presentación del libro, también se diseñaron unos marcapáginas de obsequio.

![De lo que vivo y lo que invento - Marcapáginas](dlqvylqi_3.jpg "Libro y marcapáginas en la presentación")

