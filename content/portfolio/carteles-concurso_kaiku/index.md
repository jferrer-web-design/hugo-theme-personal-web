---
title: Concurso Kaiku Caffè Latte
description: Concurso Kaiku Caffè Latte
date: "2020-04-02T19:47:09+02:00"
jobDate: 2020
work: [proyecto personal, packaging, acuarela, tinta, digital]
designs: [acuarela, tinta]
techs: [photoshop]
category: "publicidad"
thumbnail: carteles-concurso_kaiku/concurso_kaiku_0_800x400.jpg
---

**Ilustraciones para el concurso de diseño de cups para Kaiku Caffè Latte.**

Para cada variedad del producto, se presentó un diseño diferente.

![Concurso Kaiku - Espresso](concurso_kaiku_3_espresso.jpg "Diseño para la variedad Espresso")

![Concurso Kaiku - Boceto Espresso](concurso_kaiku_4_boceto_espresso.jpg "Ilustración preliminar para el diseño Espresso")

![Concurso Kaiku - Maquetado Espresso](concurso_kaiku_4_1_maquetado_espresso.jpg "Ilustración maquetada para el diseño Espresso")

![Concurso Kaiku - Macchiato](concurso_kaiku_1_macchiato.jpg "Diseño para la variedad Macchiato")

![Concurso Kaiku - Espresso](concurso_kaiku_2_cappuccino.jpg "Diseño para la variedad Cappuccino")

#### Propuesta alternativa - Vacas

![Concurso Kaiku - Espresso](concurso_kaiku_7_vaca_espresso.jpg "Diseño para la variedad Espresso")

![Concurso Kaiku - Macchiato](concurso_kaiku_5_vaca_macchiato.jpg "Diseño para la variedad Macchiato")

![Concurso Kaiku - Espresso](concurso_kaiku_6_vaca_cappuccino.jpg "Diseño para la variedad Cappuccino")



