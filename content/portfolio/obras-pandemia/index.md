---
title: Serie Pandemia
description: Serie Pandemia
date: "2020-11-02T19:47:09+02:00"
jobDate: 2020
work: [ilustraciones, dibujos, pinturas]
designs: [acuarela, tinta]
# techs: [photoshop]
category: "obras"
thumbnail: obras-pandemia/pandemia_0_800x400.jpg
---

**Obras inspiradas por la pandemia de COVID19: el miedo, la soledad, la esperanza... Todas esas sensaciones por las que hemos pasado en los meses de encierro en 2020.**

![Serie Pandemia - Diente de león](pandemia_1_diente_leon.jpg "Diente de León")

![Serie Pandemia - Pulmones](pandemia_2_pulmones.jpg "Pulmones")

![Serie Pandemia - Soledad](pandemia_3_soledad.jpg "Soledad")

![Serie Pandemia - Enjaulada](pandemia_4_jaula_gatos.jpg "Enjaulada")