---
title: Sobre mí
description: Informacion sobre mí
images: ["/img/img_jaula_small.png"]
---

#### **Soy Silvia Marecos Medina, artista, ilustradora y diseñadora.**

Nací en Paraguay, aunque resido en Valencia (España) desde hace más de una
década.

En los últimos años me he interesado principalmente por la ilustración y el
diseño gráfico. Mis proyectos más recientes abarcan el diseño de portadas de
libros, carteles, publicidad y prensa, así como libros ilustrados tanto
infantiles como para adultos.

Expongo a menudo en exposiciones colectivas de temáticas diversas y participo
puntualmente en eventos varios donde hago ilustración en vivo.

Mi trabajo se caracteriza por la presencia recurrente de la sensualidad y la
naturaleza (mis raíces amazónicas), los colores vivos y algunos toques
surrealistas. En mis trabajos son frecuentes el grafito, el carboncillo, la
acuarela y la tinta china, que luego trabajo digitalmente para sacarles el
máximo partido, combinarlos y maquetarlos.

Me encanta adentrarme en nuevos terrenos y dar vida a cada obra que realizo.
Dibujar me transporta siempre en un viaje inesperado que nunca sé adónde me va
a llevar.

Además soy Profesora en Artes Plásticas por el Instituto Superior de Bellas
Artes de Asunción (Paraguay) y disfruto viendo a los niños dar rienda suelta a
su imaginación sobre el papel.

Si te interesa mi trabajo estaré encantada de conocerte.


## Contactar conmigo

Puedes enviarme un mensaje por
[Instagram](https://www.instagram.com/smm_ilus/?hl=es)
o [Facebook](https://www.facebook.com/Smm_ilustraciones).

También puedes ponerte en contacto conmigo por **email** rellenando este
formulario.

{{< add-contact-form >}}


## Exposiciones y eventos recientes

&nbsp;  
**Exposición colectiva "Flash figures - Celebrating the figure on paper"**  
en [The RW Project](https://kristingracie.com/the-rw-project), Valencia (2020)  
[*&#8594; ver en mi blog*]({{< relref "/post/expos/202002-flash-figures.md" >}})  
&nbsp;

**Exposición EROS X 3** (con Coco Du Rong y Griselda Roces)  
en [Sankofa Espai Intercultural](https://sovint.org/), Valencia (2020)  
[*&#8594; ver en mi blog*]({{< relref "/post/expos/202001-erosx3.md" >}})  
&nbsp;

**Animación cultural "Tardeos Eróticos" (2019-2020)**  
* Dibujo en vivo durante las lecturas  
* Diseño de imagen de los carteles
* Exposición de dibujos eróticos  
&nbsp;

**Exposición colectiva "Pintar la Música"**  
en el [Conservatorio Municipal de Música José Iturbi](https://www.facebook.com/IturbiVLC), Valencia (2019)  
[*&#8594; ver en mi blog*]({{< relref "/post/expos/201906-sinestesia.md" >}})  
&nbsp;

**AEFEST - 1er Festival de Artes Eróticas de Valencia**  
en [Jarit](https://www.jarit.org/) y [Lluerna Espai Sociocultural](https://es-es.facebook.com/lluernaespai/), Valencia (2019)  
[*&#8594; ver en mi blog*]({{< relref "/post/expos/201904-aefest.md" >}})  
&nbsp;

**Exposición colectiva "Mercat Central Gaia"**  
en el Mercado Central de Valencia (2018)  
&nbsp;

